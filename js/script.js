// Задание
// Написать реализацию кнопки "Показать пароль".
//
//     Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
  const iconEye = document.getElementById('eye');
    console.log(iconEye);


  const inputPass = document.getElementById('pass');
    console.log(inputPass);


    iconEye.addEventListener("click", function() {

            if (inputPass.type === 'text') {
                inputPass.setAttribute('type', 'password');
                iconEye.setAttribute('class', 'fas fa-eye-slash icon-password')
            }else {
                inputPass.type = 'text';
                iconEye.setAttribute('class', 'fas fa-eye icon-password')
            }
    });


    const iconEyeSlash = document.getElementById('eye-slash');
    console.log(iconEyeSlash);


    const inputRePass = document.getElementById('rePass');
    console.log(inputRePass);


    iconEyeSlash.addEventListener("click", function() {

    if (inputRePass.type === 'password') {
        inputRePass.setAttribute('type', 'text');
        iconEyeSlash.setAttribute('class', 'fas fa-eye icon-password');

    }else {
        inputRePass.type = 'password';
        iconEyeSlash.setAttribute('class', 'fas fa-eye-slash icon-password');
    }
    });

    const btn = document.getElementById('submit');
    console.dir(btn);

    btn.addEventListener('click', () => {
        if (inputPass.value === inputRePass.value) {
            alert('You are welcome');
        } else {
            const error = document.getElementById('password_error');
            inputPass.style.border = "2px solid red";
            inputRePass.style.border = "2px solid red";
            error.innerText = "Нужно ввести одинаковые значения";
            btn.preventDefault();
        }
    });

